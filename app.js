const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');
const path = require('path');
const serverless = require('serverless-http');

const app = express();
// use port 3000 unless there exists a preconfigured port
var port = process.env.PORT || 3000;

app.use(cors());

// use the express-static middleware
app.use(express.static("public"))




app.get("/", function (req, res) {
  res.send("<h1>Hello World!</h1>")
})

app.get('/v1/news/categories', (req, res) => {
  const categories = [
    {
      name: 'Trending',
      slug: 'trending',
      id: 'trending',
    },
    {
      name: 'Must Reads',
      slug: 'mustReads',
      id: 'mustReads',
    }
  ];

  res.send({ categories });
});

app.get('/v1/news/todays-pick', (req, res) => {
  const { query } = req;

  let limit = 10;
  let skip = 0;

  if (query.limit) limit = query.limit;
  if (query.skip) skip = query.skip;


  fetch("https://api.readwynk.com/news", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `{
        todaysPick(limit:${limit} skip:${skip}) {
          id
          source {
            name
            description
            country
            url
          }
          title
          description
          urlToImage
          language
          url
        }
      }`
    })
  })
    .then(result => {
      return result.json();
    })
    .then(data => {
      res.send(data);
    });
});

app.get('/v1/news/trending', (req, res) => {
  const { query } = req;

  let limit = 10;
  let skip = 0;

  if (query.limit) limit = query.limit;
  if (query.skip) skip = query.skip;


  fetch("https://api.readwynk.com/news", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `{
        trending(limit:${limit} skip:${skip}) {
          id
          source {
            name
            description
            country
            url
          }
          title
          description
          urlToImage
          language
          url
        }
      }`
    })
  })
    .then(result => {
      return result.json();
    })
    .then(data => {
      res.send(data);
    });
});


app.get('/v1/news/mustReads', (req, res) => {
  const { query } = req;

  let limit = 10;
  let skip = 0;

  if (query.limit) limit = query.limit;
  if (query.skip) skip = query.skip;

  fetch("https://api.readwynk.com/news", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `{
        mustReads(limit:${limit} skip:${skip}) {
          scoops(limit:${limit} skip:${skip}) {
            id
            source {
              name
              description
              country
              url
            }
            title
            description
            urlToImage
            language
            url
          }
        }
      }`
    })
  })
    .then(result => {
      return result.json();
    })
    .then(data => {
      res.send({ data: { mustReads: data.data.mustReads.scoops }});
    });
});



app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
